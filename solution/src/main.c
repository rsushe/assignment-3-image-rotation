#include "bmp/bmp_reader.h"
#include "bmp/bmp_writer.h"
#include "file/file_processor.h"
#include "image_rotation/image_rotate.h"
#include "stdio.h"
#include "stdlib.h"

#define ERROR_CODE 1

void print_error(char *error_message) {
    fprintf(stderr, "%s\n", error_message);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        print_error("Your input should look like this: ./image-transformer <source-image> <transformed-image>");
        return ERROR_CODE;
    }

    const char *source_image_name = argv[1];
    const char *transformed_image_name = argv[2];

    FILE *source_image, *transformed_image;
    enum status open_status = open_file(&source_image, source_image_name, "rb");
    if (open_status == ERROR) {
        print_error("source image file doesn't exist");
        return ERROR_CODE;
    }

    enum status close_status = open_file(&transformed_image, transformed_image_name, "wb");
    if (close_status == ERROR) {
        print_error("transformed image file doesn't exist");
        return ERROR_CODE;
    }

    struct image *source = malloc(SIZE_OF_IMAGE_STRUCT);
    enum read_status status = from_bmp(source_image, source);

    if (status) {
        close_file(source_image);
        close_file(transformed_image);
        free(source -> data);
        print_error("error while reading image");
        return ERROR_CODE;
    }

    struct image transformed = rotate(*source);
    free(source -> data);
    free(source);

    to_bmp(transformed_image, &transformed);

    free(transformed.data);
    fclose(source_image);
    fclose(transformed_image);
    return 0;
}
