//
// Created by rsushe on 12/1/22.
//
#include "structures/image_struct.h"

int get_padding_by_image_width(uint32_t imageWidth) {
    int padding = (DIVISOR - (int) ((imageWidth * SIZE_OF_PIXEL_STRUCT) % DIVISOR)) % DIVISOR;
    return padding;
}
