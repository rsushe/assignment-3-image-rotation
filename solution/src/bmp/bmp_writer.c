//
// Created by rsushe on 11/9/22.
//
#include "bmp/bmp_writer.h"
#include "stdio.h"
#include "structures/bmp_struct.h"

#define DEFAULT_TYPE 19778
#define DEFAULT_SIZE 40
#define DEFAULT_BIT_COUNT 24
#define DEFAULT_X_PELS_PER_METER 2834
#define DEFAULT_Y_PELS_PER_METER 2834

struct bmp_header get_header_from_image(struct image const *img, int padding) {
    struct bmp_header header = {
            .bfType = DEFAULT_TYPE,
            .bfReserved = 0,
            .bOffBits = SIZE_OF_BMP_HEADER_STRUCT,
            .biSize = DEFAULT_SIZE,
            .biPlanes = 0,
            .biBitCount = DEFAULT_BIT_COUNT,
            .biCompression = 0,
            .biXPelsPerMeter = DEFAULT_X_PELS_PER_METER,
            .biYPelsPerMeter = DEFAULT_Y_PELS_PER_METER,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biSizeImage = header.biHeight * (header.biWidth + padding) * SIZE_OF_PIXEL_STRUCT;
    header.bfileSize = header.bOffBits + header.biSizeImage;
    return header;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (!out || !img) {
        return WRITE_ERROR;
    }

    uint64_t imageWidth = img->width, imageHeight = img->height;

    int padding = get_padding_by_image_width(imageWidth);
    struct bmp_header header = get_header_from_image(img, padding);

    size_t read_status = fwrite(&header, SIZE_OF_BMP_HEADER_STRUCT, 1, out);

    if (read_status != 1) {
        return WRITE_ERROR;
    }

    size_t random_byte = 0;
    for (uint64_t i = 0; i < imageHeight; i++) {
        read_status = fwrite(img->data + i * imageWidth, SIZE_OF_PIXEL_STRUCT, imageWidth, out);
        if (read_status != imageWidth) {
            return WRITE_ERROR;
        }
        read_status = fwrite(&random_byte, padding, 1, out);
        if (!read_status) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
