//
// Created by rsushe on 11/9/22.
//
#include "bmp/bmp_reader.h"
#include "stdio.h"
#include "stdlib.h"
#include "structures/bmp_struct.h"

enum read_status from_bmp(FILE *in, struct image *img) {
    if (!in || !img) {
        return READ_INVALID_ARGUMENTS;
    }
    struct bmp_header header;
    size_t read_status = fread(&header, SIZE_OF_BMP_HEADER_STRUCT, 1, in);
    printf("%zu\n", read_status);
    if (read_status != 1) {
        return READ_INVALID_HEADER;
    }
    uint32_t imageWidth = header.biWidth, imageHeight = header.biHeight;
    img->data = malloc(SIZE_OF_PIXEL_STRUCT * imageWidth * imageHeight);
    img->width = imageWidth;
    img->height = imageHeight;

    int padding = get_padding_by_image_width(imageWidth);
    fseek(in, header.bOffBits, SEEK_SET);

    for (uint32_t i = 0; i < imageHeight; i++) {
        read_status = fread(img->data + i * imageWidth, SIZE_OF_PIXEL_STRUCT, imageWidth, in);
        if (read_status != imageWidth) {
            return READ_INVALID_BITS;
        }
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}
