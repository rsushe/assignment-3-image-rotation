//
// Created by rsushe on 11/16/22.
//
#include "file/file_processor.h"

enum status open_file(FILE** file, const char* image_file_name, const char* read_mode) {
    if (!file || !image_file_name || !read_mode) {
        return ERROR;
    }
    *file = fopen(image_file_name, read_mode);
    if (!*file) {
        return ERROR;
    }
    return SUCCESS;
}

enum status close_file(FILE* file) {
    if (!file) {
        return ERROR;
    }
    if (fclose(file)) {
        return ERROR;
    }
    return SUCCESS;
}
