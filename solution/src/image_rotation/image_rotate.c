//
// Created by rsushe on 11/9/22.
//
#include "image_rotation/image_rotate.h"
#include "stdlib.h"

struct image rotate( const struct image source ) {
    uint64_t height = source.height, width = source.width;

    struct image transformed;
    transformed.height = width;
    transformed.width = height;
    transformed.data = malloc(SIZE_OF_PIXEL_STRUCT * width * height);

    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            transformed.data[j * height + height - i - 1] = source.data[i * width + j];
        }
    }

    return transformed;
}
