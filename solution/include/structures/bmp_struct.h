//
// Created by rsushe on 11/9/22.
//

#ifndef IMAGE_TRANSFORMER_BMP_STRUCT_H
#define IMAGE_TRANSFORMER_BMP_STRUCT_H

#include  <stdint.h>

#define SIZE_OF_BMP_HEADER_STRUCT sizeof(struct bmp_header)

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

#endif //IMAGE_TRANSFORMER_BMP_STRUCT_H
