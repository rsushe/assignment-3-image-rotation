//
// Created by rsushe on 11/9/22.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_STRUCT_H
#define IMAGE_TRANSFORMER_IMAGE_STRUCT_H

#include "inttypes.h"

#define SIZE_OF_IMAGE_STRUCT sizeof(struct image)
#define SIZE_OF_PIXEL_STRUCT sizeof(struct pixel)
#define DIVISOR 4

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
} __attribute__((packed));

int get_padding_by_image_width(uint32_t imageWidth);

#endif //IMAGE_TRANSFORMER_IMAGE_STRUCT_H
