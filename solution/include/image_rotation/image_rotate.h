//
// Created by rsushe on 11/9/22.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_ROTATE_H
#define IMAGE_TRANSFORMER_IMAGE_ROTATE_H

#include "structures/image_struct.h"

struct image rotate(struct image source);

#endif //IMAGE_TRANSFORMER_IMAGE_ROTATE_H
