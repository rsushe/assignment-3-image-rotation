//
// Created by rsushe on 11/9/22.
//

#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H

#include "stdio.h"
#include "structures/image_struct.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_ARGUMENTS
};

enum read_status from_bmp( FILE* in, struct image* img );

#endif //IMAGE_TRANSFORMER_BMP_READER_H
