//
// Created by rsushe on 11/16/22.
//

#ifndef IMAGE_TRANSFORMER_FILE_PROCESSOR_H
#define IMAGE_TRANSFORMER_FILE_PROCESSOR_H

#include "stdio.h"

enum status {
    SUCCESS,
    ERROR
};

enum status open_file(FILE** file, const char* image_file_name, const char* read_mode);

enum status close_file(FILE* file);

#endif //IMAGE_TRANSFORMER_FILE_PROCESSOR_H
